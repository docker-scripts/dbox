#!/bin/bash -x

source /host/settings.sh
DOMAIN=${DOMAIN:-$IMAGE.example.org}

### create a configuration file
cat <<EOF > /etc/apache2/sites-available/lbd.conf
<VirtualHost *:80>
        ServerName $DOMAIN
        RedirectPermanent / https://$DOMAIN/
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $DOMAIN

        DocumentRoot /var/www/lbd
        <Directory /var/www/lbd/>
            AllowOverride All
        </Directory>

        SSLEngine on
        SSLCertificateFile        /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile   /etc/ssl/private/ssl-cert-snakeoil.key
        #SSLCertificateChainFile  /etc/ssl/certs/ssl-cert-snakeoil.pem

        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
</VirtualHost>
EOF
### we need to refer to this apache2 config by the name "$DOMAIN.conf" as well
ln /etc/apache2/sites-available/{lbd,$DOMAIN}.conf

cat <<EOF > /etc/apache2/conf-available/downloads.conf
Alias /downloads /var/www/downloads
<Directory /var/www/downloads>
    Options Indexes FollowSymLinks
</Directory>
EOF

### enable ssl etc.
a2enmod ssl
a2dissite 000-default
a2ensite lbd
a2enmod headers rewrite
a2enconf downloads

### limit the memory size of apache2 when developing
if [[ -n $DEV ]]; then
    sed -i /etc/php/8.2/apache2/php.ini \
        -e '/^\[PHP\]/ a apc.rfc1867 = 1' \
        -e '/^display_errors/ c display_errors = On'

    sed -i /etc/apache2/mods-available/mpm_prefork.conf \
        -e '/^<IfModule/,+5 s/StartServers.*/StartServers 2/' \
        -e '/^<IfModule/,+5 s/MinSpareServers.*/MinSpareServers 2/' \
        -e '/^<IfModule/,+5 s/MaxSpareServers.*/MaxSpareServers 4/' \
        -e '/^<IfModule/,+5 s/MaxRequestWorkers.*/MaxRequestWorkers 50/'
fi

### modify the configuration of php
cat <<EOF > /etc/php/8.2/mods-available/apcu.ini
extension=apcu.so
apcu.mmap_file_mask=/tmp/apcu.XXXXXX
apcu.shm_size=96M
EOF

sed -i /etc/php/8.2/apache2/php.ini \
    -e '/error_reporting/ c error_reporting = E_ERROR' \
    -e '/memory_limit/ c memory_limit = 256M' \
    -e '/post_max_size/ c post_max_size = 16M' \
    -e '/max_input_vars/ c max_input_vars = 1500' \
    -e '/upload_max_filesize/ c upload_max_filesize = 100M' \
    -e '/max_execution_time/ c max_execution_time = 90' \
    -e '/default_socket_timeout/ c default_socket_timeout = 90'

service apache2 restart

sed -i /etc/php/8.2/cli/php.ini \
    -e '/error_reporting/ c error_reporting = E_ERROR' \
    -e '/max_execution_time/ c max_execution_time = 0' \
    -e '/default_socket_timeout/ c default_socket_timeout = -1'
