APP=labdoo/ds
DOMAIN="lbd.example.org"
CONTAINER=$DOMAIN

### Uncomment if this installation is for development.
DEV=true

### Other domains.
[[ -n $DEV ]] && DOMAINS="dev.$DOMAIN tst.$DOMAIN"

### Admin settings.
ADMIN_PASS=123456
ADMIN_EMAIL=admin@example.org

### DB settings
DBHOST=mariadb
DBPORT=3306
DBNAME=${DOMAIN//./_}
DBUSER=${DOMAIN//./_}
DBPASS=${DOMAIN//./_}

### Uncomment REDIS_HOST to enable Drupal Redis module. Change hostname if needed.
### You must have a server running Redis to use this feature, for example install
### 'redis' docker-scripts container: https://gitlab.com/docker-scripts/redis
#REDIS_HOST=redis
#REDIS_PASS='ohhebahQuahghaingeef1ifeitah5yei'
